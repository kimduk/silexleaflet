<?php
// web/index.php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

// definitions

$app['debug'] = true;

$app->get('/', function() use ($app){
    return $app->sendFile('../static/index.html');
});

$app->get('/css/{filename}', function($filename) use ($app) {
    if(!file_exists('../static/scc/' . $filename)) {
        $app->abort(404);
    }
    return $app->sendFile('../static/css/' . $filename, 200, array('Content-Type' => 'text/css'));
});

$app->get('/parks/within', function() use ($app) {
    $db_name = 'parks';
    $db_connection = "mongodb://localhost:27017/" . $db_name;
    $client = new MongoClient($db_connection);
    $db = $client->selectDB($db_name);
    $parks = new MongoCollection($db, 'parks');

    $lat1 = floatval($app->escape($_GET['lat1']));
    $lat2 = floatval($app->escape($_GET['lat2']));
    $lon1 = floatval($app->escape($_GET['lon1']));
    $lon2 = floatval($app->escape($_GET['lon2']));

    $bound_box_query =  array( 'pos' =>
        array( '$within' =>
            array( '$box' =>
                array(  array( $lon1, $lat1),
                    array( $lon2, $lat2)))));
    $result = $parks->find($bound_box_query);

    $response = "[";
    foreach ($result as $park){
        $response .= json_encode($park);
        if( $result->hasNext()){ $response .= ","; }
    }
    $response .= "]";

    return $app->json(json_decode($response));
});

$app->run();